variable "AWS_ACCESS_KEY" {
}

variable "AWS_SECRET_KEY" {
}

variable "AWS_REGION" {
  default = "ap-southeast-1"
}

data "aws_vpcs" "show-vpcs" {
}

data "aws_security_groups" "show-sec-grps" {
  filter {
    name = "vpc-id"
    values = [data.aws_vpc.show-default-vpc.id]
  }
}

data "aws_vpc" "show-default-vpc" {
  default = true
}