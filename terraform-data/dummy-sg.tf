
resource "aws_security_group" "allow-http" {
  name = "terraform-allow-http"
  description = "this is dummy sec group creation with terraform"
  vpc_id = data.aws_vpc.show-default-vpc.id
}
