provider "aws" {
  alias = "Primary" #alias to refer to primary site
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
  region     = "ap-southeast-1"
}

provider "aws" {
  alias = "Secondary" #alias to refer to secondary site
  access_key = var.AWS_ACCESS_KEY
  secret_key = var.AWS_SECRET_KEY
  region = "us-east-1"
}