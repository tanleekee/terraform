resource aws_instance primary-ec2 {
  provider = aws.Primary
  ami           = var.AMIS[var.PRI_AWS_REGION]
  instance_type = "t2.micro"
  count = 2
}

resource "aws_instance" "secondary-ec2" {
  provider = aws.Secondary
  ami           = var.AMIS[var.SEC_AWS_REGION]
  instance_type = "t2.micro"
  count = 1
}