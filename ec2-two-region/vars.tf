variable "AWS_ACCESS_KEY" {
}

variable "AWS_SECRET_KEY" {
}

variable "PRI_AWS_REGION" {
  default = "ap-southeast-1"
}

variable "SEC_AWS_REGION" {
  default = "us-east-1"
}

variable "AMIS" {
  type = map(string)
  default = {
    ap-southeast-1 = "ami-01581ffba3821cdf3"
    us-east-1 = "ami-042e8287309f5df03"
  }
}