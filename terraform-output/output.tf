output "sec-grp-arn" {
  value = aws_security_group.allow-http.arn
}

output "sec-grp-id" {
  value = aws_security_group.allow-http.id
}